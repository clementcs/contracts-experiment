module.exports = {
    rpc: {
	host:"localhost",
	port:8543
    },
    networks: {
	development: {
	    host: "localhost", 
	    port: 8543,
	    network_id: "*",
	    from:"0xb595a97bc6039095fc3b6aa165e75a4ee79959e4"
	    
	}
    }
};
