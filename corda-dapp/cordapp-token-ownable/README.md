# CorDapp - Token CorDapp using OwnableState

## Overview

Copied from https://github.com/amolpednekar/cordapp-token-ownable.
With a few modifications (allowing ssh access for easier testing).

## Usage

Use easily with the following script [https://github.com/klementc/singularityRecipes/blob/master/corda_start.sh](https://github.com/klementc/singularityRecipes/blob/master/corda_start.sh)